<?php

namespace Drupal\novaposhta_tracking;

use Drupal\novaposhta\API\NovaPoshtaAPI;

class getResults{

	protected $API;

	function __construct(){
		$this->API = new NovaPoshtaAPI(NULL);
	}
	public function getWrapID(){
		return 'novaposhta_tracking_result_wrap';
	}
	public function info($EnNum = NULL){
		$info = NULL;
		if(!empty($EnNum)){
			$info = $this->API->getTrackingDocument([
				'Documents'		=> [
					[
						'DocumentNumber'	=> trim($EnNum),
						'Phone'				=> ''
					]
				]
			]);
		}
		return [
			'#theme'		=> 'novaposhta_tracking_result',
			'#info'			=> $info,
			'#prefix'		=> '<div id="'.$this->getWrapID().'">',
			'#suffix'		=> '</div>'
		];
	}
}