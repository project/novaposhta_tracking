<?php

namespace Drupal\novaposhta_tracking\Controller;

use Drupal\novaposhta_tracking\getResults;
use Drupal\novaposhta_tracking\Form\novaposhtaTrackingForm;

class novaposhtaTrackingUser{
	public static function page(){
		$results = new getResults(NULL);
		return [
			'#theme'		=> 'novaposhta_tracking_user_page',
			'#info'			=> [
				'form'			=> \Drupal::formBuilder()->getForm(new novaposhtaTrackingForm()),
				'results'		=> $results->info( ( !empty($_REQUEST['ttn']) ? trim($_REQUEST['ttn']) : '' ) )
			]
		];
	}
}