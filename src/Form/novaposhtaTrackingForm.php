<?php

namespace Drupal\novaposhta_tracking\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\novaposhta_tracking\getResults;

class novaposhtaTrackingForm extends FormBase{

	protected $novaPoshta;
	protected $isPopup;
	protected $basketPopup;

	function __construct($isPopup = FALSE){
		$this->novaPoshta = \Drupal::service('NovaPoshta');
		$this->isPopup = $isPopup;
		$this->basketPopup = \Drupal::service('BasketPopup');
	}
	public function getFormId(){
		return 'novaposhta_tracking_form';
	}
 
	public function buildForm(array $form, FormStateInterface $form_state){
		$form += [
			'#prefix'		=> '<div id="novaposhta_tracking_form_ajax_wrap">',
			'#suffix'		=> '</div>'
		];
		$form['ttn'] = [
			'#type'			=> 'textfield',
			'#title'		=> $this->novaPoshta->t('Enter invoice number', [], 'novaposhta_tracking'),
			'#required'		=> TRUE,
			'#default_value' => !empty($_REQUEST['ttn']) ? trim($_REQUEST['ttn']) : ''
		];
		$form['actions'] = [
			'#type'			=> 'actions',
			'submit'		=> [
				'#type'			=> 'submit',
				'#value'		=> $this->novaPoshta->t('Track', [], 'novaposhta_tracking'),
				'#ajax'			=> [
					'wrapper'		=> 'novaposhta_tracking_form_ajax_wrap',
					'callback'		=> [$this, 'ajaxCallback']
				]
			]
		];
		return $form;
	}
 
	public function submitForm(array &$form, FormStateInterface $form_state){}
 
	public function ajaxCallback(array $form, FormStateInterface $form_state){
		if ($form_state->isSubmitted() && !$form_state->getErrors()){
			$response = new AjaxResponse();
			$results = new getResults();
			$result = $results->info($form_state->getValue('ttn'));
			if(!empty($this->isPopup)){
				$this->basketPopup->isSite(TRUE);
				$this->basketPopup->openModal(
					$response,
					$this->novaPoshta->t('Track order', [], 'novaposhta_tracking'),
					$result,[
						'width' => 400,
						'class' => ['novaposhta_tracking_result']
					]
				);
			} else {
				$response->addCommand(new ReplaceCommand(
					'#'.$results->getWrapID(),
					$result
				));
			}
			return $response;
		}
		return $form;
	}
	
}
