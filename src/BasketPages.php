<?php

namespace Drupal\novaposhta_tracking;

use Drupal\novaposhta_tracking\Form\novaposhtaTrackingForm;

class BasketPages{
	public static function alter(&$element, $page_type, $page_subtype){
		switch($page_type){
			case'api':
				switch($page_subtype){
					case'np_tracking_popup':
						$BasketPopup = \Drupal::service('BasketPopup');
						$BasketPopup->isSite(TRUE);
						if(!empty($_POST['ttn_load'])){
							$results = new getResults();
							$result = $results->info($_POST['ttn_load']);
							$BasketPopup->openModal(
								$element,
								\Drupal::service('NovaPoshta')->t('Track order', [], 'novaposhta_tracking'),
								$result,[
									'width' => 400,
									'class' => ['novaposhta_tracking_result']
								]
							);
						} else {
							$BasketPopup->openModal(
								$element,
								\Drupal::service('NovaPoshta')->t('Track order', [], 'novaposhta_tracking'),
								\Drupal::formBuilder()->getForm(new novaposhtaTrackingForm(TRUE)),[
									'width' => 400,
									'class' => ['novaposhta_tracking_form']
								]
							);
						}
					break;
				}
			break;
		}
	}
}
